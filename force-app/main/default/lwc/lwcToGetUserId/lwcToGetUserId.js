import { LightningElement } from 'lwc';
import Id from '@salesforce/user/Id';

// Created by Dhana Prasad 
export default class LwcToGetUserId extends LightningElement {
    userId = Id;
}